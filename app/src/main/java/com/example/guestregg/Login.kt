package com.example.guestregg

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity(), CoroutineScope by MainScope() {
    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        job = Job()

        loginButton.setOnClickListener {
            launch {
                try {
                    val token = withContext(Dispatchers.IO) {
                        Backend.login(User(userName.text.toString(), password.text.toString()))
                            .let {
                                it.token
                            }
                    }

                    setResult(Activity.RESULT_OK, Intent().also {
                        it.putExtra("token", token)
                    })
                    finish()

                } catch (e: Throwable) {
                    Log.e("login", e.message ?: "Unexpected error")
                }
            }
        }

        setTitle("login")
    }


    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}
