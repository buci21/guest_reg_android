package com.example.guestregg

import android.app.Activity
import android.content.Context
import android.content.Intent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class MainActivity : AppCompatActivity(), CoroutineScope by MainScope() {
    private val loginRequest = 1

    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        job = Job()

        val token = getPreferences(Context.MODE_PRIVATE).let {
            it.getString("token", null)
        }

        if (token == null) {
            Intent(this, Login::class.java).also { loginIntent ->
                startActivityForResult(loginIntent, loginRequest)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (requestCode == loginRequest) {
            if (resultCode == Activity.RESULT_OK) {
                intent?.also { data ->
                    getPreferences(Context.MODE_PRIVATE).also {
                        with(it.edit()) {
                            putString("token", data.getStringExtra("token"))
                            commit()
                        }
                    }

                    Backend.token = data.getStringExtra("token")
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}
