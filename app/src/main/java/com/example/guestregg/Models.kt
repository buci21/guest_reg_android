package com.example.guestregg

import java.util.*

data class RegistrationEntry(
    val id: Int,
    val firstName: String,
    val lastName: String,
    val zipCode: String,
    val city: String,
    val country: String,
    val street: String,
    val nature: String,
    val building: String,
    val dateOfBirth: Date,
    val passportNumber: String
)

data class User(
    val username: String,
    val password: String
)

data class TokenResponse(
    val token: String
)