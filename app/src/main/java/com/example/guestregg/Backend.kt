package com.example.guestregg

import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import retrofit2.create


interface API {
    @POST("/login")
    suspend fun login(@Body user: User): TokenResponse

    @POST("/register")
    suspend fun register(@Body user: User)

    @POST("/submit")
    suspend fun submit(
        @Header("Authorization") token: String,
        @Body entry: RegistrationEntry
    )
}

object Backend {
    private const val API_URL = "http://10.0.2.2"

    private val instance by lazy {
        Retrofit.Builder()
            .baseUrl(API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create<API>()
    }

    var token: String? = null;

    suspend fun login(user: User) = instance.login(user)
    suspend fun register(user: User) = instance.register(user)
    suspend fun submit(entry: RegistrationEntry) = instance.submit(token!!, entry)
}